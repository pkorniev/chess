<?php

use yii\db\Migration;

/**
 * Class m210624_071613_migration_1
 */
class m210624_071613_migration_1 extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210624_071613_migration_1 cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        echo $this->db->driverName;
    }

    public function down()
    {
        echo "m210624_071613_migration_1 cannot be reverted.\n";

        return false;
    }

}
