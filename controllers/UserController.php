<?php
/**
 * Created by PhpStorm.
 * User: KPA
 * Date: 15.08.16
 * Time: 11:53
 */

namespace app\controllers;


use yii\rest\ActiveController;

class UserController extends  ActiveController
{
    public $modelClass = 'app\models\User';

}