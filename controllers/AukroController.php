<?php
/**
 * Created by PhpStorm.
 * User: KPA
 * Date: 15.08.16
 * Time: 11:36
 */

namespace app\controllers;


use yii\rest\ActiveController;

class AukroController extends ActiveController
{
    public $modelClass = 'app\models\Aukro';
}