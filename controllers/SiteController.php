<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use pkorniev\TelegramBotSimple\Bot;
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
//        var_dump(Yii::$app->translator->getHideLangUrl());
/*        $homeUrl = Yii::$app->getHomeUrl();
        Yii::$app->setHomeUrl(Yii::$app->urlManager->createUrl($homeUrl));*/
//        return $this->goHome();
        print_r(Yii::$app->language);

        $ftpAddr = '10.99.99.32';
        $ftpLogin = '1c';
        $ftpPassword = 'zqxwce1a@s2!d3';

        $conn_id = ftp_connect($ftpAddr);

// вход с именем пользователя и паролем
        $login_result = ftp_login($conn_id, $ftpLogin, $ftpPassword);

// проверка соединения
        if ((!$conn_id) || (!$login_result)) {
            echo "Не удалось установить соединение с FTP-сервером!";
            echo "Попытка подключения к серверу $ftpAddr была произведена под именем $ftpLogin";
            exit;
        } else {
            echo "Установлено соединение с FTP сервером $ftpAddr под именем $ftpLogin";
        }
        ftp_pasv($conn_id, true);

        $contents = ftp_nlist($conn_id, "2021/8");


// вывод $contents
        var_dump($contents);




        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
