<?php

namespace app\controllers;
use app\models\Board;
use app\models\Customer;
use app\models\LoginForm;
use app\models\Piece;
use app\models\Square;
use app\models\User_db;
use Yii;
use app\models\RegForm;
use app\models\User;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\db\QueryBuilder;
use yii\data\Pagination;

class LearnController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'registration','removeuser'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'registration'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout','removeuser'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
//        var_dump(Yii::$app->language);
        $board = new Board();
        $board->setStartPosition();
        $position = $board->getPosition();
        foreach ($position as $rowId => $row) {
            foreach ($row as $squareId => $square) {
                /** @var Square $square */
                $roleArray = [1 => 'Pawn',2 => 'Bishop',3 => 'Knight', 4 => 'Rook', 5=> 'Queen', 6 => 'King'];
                $colorArray = [1 => 'W-',2 => 'B-'];
                if ($square && $square->piece){
                    echo  $square->name.':'.$colorArray[$square->piece->color]. $roleArray[$square->piece->role].' ';
                    if ($square->name == 'd4') {
                       echo '<pre>';
//                        var_dump($square->color);
                        print_r($square->piece->getAllowedMoves($position));die();
                    }
                }
                else echo  '0 ';
            }
            echo '<br>';
        }
//        var_dump($board->getPosition());
            die();
        return $this->render('index');
    }

        public function actionRegistration()
    {
        $model = new RegForm();
        if($model->load(Yii::$app->request->post()))
        {
            if (!(User::findByUserName($model->email)))
            {
                $user = new User();
                $user->email = $model->email;
                $user->password = md5($model->password);
                $user->insert();
                return $this->redirect(['index']);
            }
            else
            {
                $model->addError('email', 'This email is already exist!');
            }
        }
        return $this->render('registration',['model'=>$model]);
    }

    public function actionLogin()
    {
        return $this->goHome();
        $model = new LoginForm();
        if($model->load(Yii::$app->request->post()))
        {
            if (User::findByUserName($model->email))
            {

                if (User::findByPassword($model->email,md5($model->password)))
                {
                    $identity = $model->getUser();
                    Yii::$app->user->login($identity);
                    return $this->render('index',['model'=>$model]);
                }
                else
                {
                    $model->addError('password', 'Wrong password!');
                }
            }
            else if ($model->email)
            {
                $model->addError('email', 'Wrong email!');
            }
        }

        return $this->render('login',['model'=>$model]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->render('index');
    }
    public function actionAllusers(){
        {
            $query = User::find();
            $pagination = new Pagination([
                'defaultPageSize' => 10,
                'totalCount' => $query->count(),
            ]);
            $users = $query->orderBy('id')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('allusers', [
                'users' => $users,
                'pagination' => $pagination,
            ]);
        }
    }
    public  function actionRemoveuser ($id){
        if (Yii::$app->user->identity->permissions == 1 && $id!=1)
        {
            User::deleteAll(['id' => $id]);
        }
        $query = User::find();
        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);
        $users = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('allusers', [
            'users' => $users,
            'pagination' => $pagination,
        ]);
    }

}
