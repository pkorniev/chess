<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<div>
<h1>Users</h1>

<ul>
    <?php
    foreach ($users as $user): ?>
        <li><div class="btn-lg"  >
            <?= Html::encode(" id: {$user->id} | email: {$user->email}")?>
            <?php if (Yii::$app->user->identity->permissions == 1) { ?>
            <a class="btn btn-sm btn-success" href="?r=learn/removeuser&id=<?=$user->id?>">remove user</a>
                <?php } ?>
                </div>
        </li>
    <?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>
</div>