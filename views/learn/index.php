<?php ?>
<div class="site-index">

    <div class="jumbotron">
        <?php
        if (Yii::$app->user->isGuest)
        { echo  "<h1>You are Guest</h1>";?>
        <p><a class="btn btn-lg btn-success" href="?r=learn/login">Login</a></p>
        <p><a class="btn btn-lg btn-success" href="?r=/learn/registration">Registration</a></p>
        <?php } else {  echo  "<h1>You are User!</h1>";?>
        <p><a class="btn btn-lg btn-success" href="?r=learn/allusers">List of all users</a></p>
        <?php }?>

    </div>

    <div class="body-content">
    </div>
    </div>
</div>
