<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\httpclient\\' => array($vendorDir . '/yiisoft/yii2-httpclient/src'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii/src'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\codeception\\' => array($vendorDir . '/yiisoft/yii2-codeception'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap/src'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'pkorniev\\TelegramBotSimple\\' => array($vendorDir . '/pkorniev/yii2-telegram-bot-simple/src'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
);
