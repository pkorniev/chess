<?php

namespace app\models;


use yii\base\Model;
use Yii;

class Board extends Model
{
    const EMPTY_BOARD = [
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
        [null,null,null,null,null,null,null,null,],
    ];


    const A1 = 1; const A2 = 9;  const A3 = 17; const A4 = 25; const A5 = 33; const A6 = 41; const A7 = 49; const A8 = 57;
    const B1 = 2; const B2 = 10; const B3 = 18; const B4 = 26; const B5 = 34; const B6 = 42; const B7 = 50; const B8 = 58;
    const C1 = 3; const C2 = 11; const C3 = 19; const C4 = 27; const C5 = 35; const C6 = 43; const C7 = 51; const C8 = 59;
    const D1 = 4; const D2 = 12; const D3 = 20; const D4 = 28; const D5 = 36; const D6 = 44; const D7 = 52; const D8 = 60;
    const E1 = 5; const E2 = 13; const E3 = 21; const E4 = 29; const E5 = 37; const E6 = 45; const E7 = 53; const E8 = 61;
    const F1 = 6; const F2 = 14; const F3 = 22; const F4 = 30; const F5 = 38; const F6 = 46; const F7 = 54; const F8 = 62;
    const G1 = 7; const G2 = 15; const G3 = 23; const G4 = 31; const G5 = 39; const G6 = 47; const G7 = 55; const G8 = 63;
    const H1 = 8; const H2 = 16; const H3 = 24; const H4 = 32; const H5 = 40; const H6 = 48; const H7 = 56; const H8 = 64;

    const CASTLING_KINGSIDE = 1;
    const CASTLING_QUEENSIDE = 2;

    const WHITE_MOVE = 1;
    const BLACK_MOVE = 2;


    private $a1; private $a2; private $a3; private $a4; private $a5; private $a6; private $a7; private $a8;
    private $b1; private $b2; private $b3; private $b4; private $b5; private $b6; private $b7; private $b8;
    private $c1; private $c2; private $c3; private $c4; private $c5; private $c6; private $c7; private $c8;
    private $d1; private $d2; private $d3; private $d4; private $d5; private $d6; private $d7; private $d8;
    private $e1; private $e2; private $e3; private $e4; private $e5; private $e6; private $e7; private $e8;
    private $f1; private $f2; private $f3; private $f4; private $f5; private $f6; private $f7; private $f8;
    private $g1; private $g2; private $g3; private $g4; private $g5; private $g6; private $g7; private $g8;
    private $h1; private $h2; private $h3; private $h4; private $h5; private $h6; private $h7; private $h8;

    private $squaresMap = [
        ['a1','b1','c1','d1','e1','f1','g1','h1'],
        ['a2','b2','c2','d2','e2','f2','g2','h2'],
        ['a3','b3','c3','d3','e3','f3','g3','h3'],
        ['a4','b4','c4','d4','e4','f4','g4','h4'],
        ['a5','b5','c5','d5','e5','f5','g5','h5'],
        ['a6','b6','c6','d6','e6','f6','g6','h6'],
        ['a7','b7','c7','d7','e7','f7','g7','h7'],
        ['a8','b8','c8','d8','e8','f8','g8','h8'],
    ];

    private $isCheck = false;

    private $moveNumber = 0;

    private $whoMove = Piece::COLOR_WHITE;


   /* private $whiteKingRow = 0;
    private $whiteKingColumn = 0;

    private $blackKingRow = 0;
    private $blackKingColumn = 0;

    private $kingsSquares = [
        Piece::COLOR_WHITE => [0,0],
        Piece::COLOR_BLACK => [0,0],
    ];
   */

    private $identifier;

    private function setPieceSquare($square,$row,$column,$roleId,$color)
    {
        $piece = null;
        if ($roleId) {
            $piece = new Piece(['row' => $row, 'column' => $column, 'color' => $color,'role' => $roleId,]);
        }

        $this->$square = new Square(['name' => $square,'piece' => $piece,'color' => Square::getColor($row,$column)]);
    }


    public function getPosition() {

        $position = self::EMPTY_BOARD;
        foreach ($this->squaresMap as $rowKey => $row) {
            foreach ($row as $squareKey => $square) {
                $position[$rowKey][$squareKey] = $this->$square;
            }
        }
        return $position;
    }

    public function setStartPosition()
    {
        foreach ($this->squaresMap as $rowKey => $row) {
            foreach ($row as $columnKey => $square) {
                if ($rowKey == 0) {
                    if ($columnKey == 0 || $columnKey == 7)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_ROOK,Piece::COLOR_WHITE);
                    elseif ($columnKey == 1 || $columnKey == 6)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_KNIGHT,Piece::COLOR_WHITE);
                    elseif ($columnKey == 2 || $columnKey == 5)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_BISHOP,Piece::COLOR_WHITE);
                    elseif ($columnKey == 3) $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_QUEEN,Piece::COLOR_WHITE);
                    elseif ($columnKey == 4)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_KING,Piece::COLOR_WHITE);
                }
                elseif ($rowKey == 1){
                    $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_PAWN,Piece::COLOR_WHITE);
                }
                elseif ($rowKey == 6){
                    $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_PAWN,Piece::COLOR_BLACK);
                }
                elseif ($rowKey == 7){
                    if ($columnKey == 0 || $columnKey == 7)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_ROOK,Piece::COLOR_BLACK);
                    elseif ($columnKey == 1 || $columnKey == 6)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_KNIGHT,Piece::COLOR_BLACK);
                    elseif ($columnKey == 2 || $columnKey == 5)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_BISHOP,Piece::COLOR_BLACK);
                    elseif ($columnKey == 3)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_QUEEN,Piece::COLOR_BLACK);
                    elseif ($columnKey == 4)  $this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_KING,Piece::COLOR_BLACK);
                }
                else $this->setPieceSquare($square,$rowKey,$columnKey,null,null);
                if ($square == 'e7') {$this->setPieceSquare($square,$rowKey,$columnKey,null,null);}
                if ($square == 'e7') {$this->setPieceSquare($square,$rowKey,$columnKey,Piece::ROLE_QUEEN,Piece::COLOR_WHITE);}

            }
        }

        var_dump($this->checkIsCheck(Piece::COLOR_WHITE));die();
    }

    public static function initBoard($identifier)
    {
        $baseUrl = Yii::getAlias('@app/runtime/log');

    }

    public function castling($color, $castlingType)
    {

    }

    public function move($startRow,$startColumn,$endRow,$endColumn)
    {
        if ($this->checkIsValidMove($startRow,$startColumn,$endRow,$endColumn)) {
            //DO MOVE
            //TODO KING coordinates
            $startSquareName = $this->squaresMap[$startRow][$startColumn];
            $endSquareName = $this->squaresMap[$endRow][$endColumn];
            $roleId = $this->$startSquareName->piece->role;
            $color = $this->$startSquareName->piece->color;
            $this->setPieceSquare($startSquareName,$startRow,$startColumn,null,null);
            $this->setPieceSquare($endSquareName,$endRow,$endColumn,$roleId,$color);
            $this->$endSquareName->piece = $this->$startSquareName->piece;
            $this->$startSquareName->piece = null;
            if ($this->checkIsCheck($this->reverseColor($this->whoMove))) {
                //revert move;
                return false;
            }
            $this->setCheck();
            return true;
        }
        return false;
    }

    private function checkIsValidMove($startRow,$startColumn,$endRow,$endColumn)
    {
        //first step validation
        if (isset($this->squaresMap[$startRow][$startColumn]) && isset($this->squaresMap[$endRow][$endColumn])) {
            $startSquareName = $this->squaresMap[$startRow][$startColumn];
            $square = $this->$startSquareName;
            /** @var Square|null $square */
            if ($square && $square->piece->color == $this->whoMove) {
                return $square->piece->isValidMove($endRow, $endColumn,$this->getPosition());
            }
        }
        return false;
    }

    private function checkIsCheck($whoCheck)
    {
        foreach ($this->squaresMap as $row) {
            foreach ($row as $squareName) {
                $square = $this->$squareName;
                /** @var  Piece $piece */
                $piece = $square->piece;
                $position = $this->getPosition();
                $kingSquare = $this->findKing($this->reverseColor($whoCheck));
                if ($piece && $piece->color == $whoCheck ) {
                    foreach ($piece->getAllowedMoves($position,true) as $move ){
                        if ($move[0] == $kingSquare[0] && $move[1] == $kingSquare[1]) return true;
                    }
                }
            }
        }
        return false;
    }

    private function setCheck()
    {
        $this->isCheck = $this->checkIsCheck($this->whoMove);
    }

    /**
     * @param int $color
     * @return int
     */
    private function reverseColor($color)
    {
        if ($color == Piece::COLOR_WHITE) return Piece::COLOR_BLACK;
        if ($color == Piece::COLOR_BLACK) return Piece::COLOR_WHITE;
        return null;
    }

    private function findKing($kingColor)
    {
        foreach ($this->squaresMap as $rowKey => $row) {
            foreach ($row as $columnKey =>  $squareName) {
                $square = $this->$squareName;
                if ($square->piece && $square->piece->color == $kingColor && $square->piece->role == Piece::ROLE_KING) {
                    return [$rowKey,$columnKey];
                }
            }
        }
    }
}