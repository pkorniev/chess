<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


class User extends ActiveRecord implements IdentityInterface
{
    //public $permissions;
    //public $id;
    //public $email;
   // public $password;
    //public $auth_key;    //Почему то не записівает в базу если обьявить свойства ????

    public static function tableName()
    {
        return 'user';
    }
        /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findByUserName($email)
    {
            $users = User::find()->all();
            foreach ($users as $user) {
                if (strcasecmp($user['email'], $email) === 0) {
                    return new static($user);
                }
            }
            return null;
    }
    public static function findByPassword($email, $password)
    {
        $user = User::findOne([
            'email' => $email
        ]);
                if (strcasecmp($user->password, $password) === 0) {
                return true;
            }
            else {
                return false;
            }

    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}
