<?php


namespace app\models;


use yii\base\Model;

/**
 * @var Piece $piece
 *
 * Class Square
 * @package app\models
 */
class Square extends Model
{
    const SQUARE_COLOR_WHITE = 1;
    const SQUARE_COLOR_BLACK = 2;

    /*
    const A1 = 1; const A2 = 9;  const A3 = 17; const A4 = 25; const A5 = 33; const A6 = 41; const A7 = 49; const A8 = 57;
    const B1 = 2; const B2 = 10; const B3 = 18; const B4 = 26; const B5 = 34; const B6 = 42; const B7 = 50; const B8 = 58;
    const C1 = 3; const C2 = 11; const C3 = 19; const C4 = 27; const C5 = 35; const C6 = 43; const C7 = 51; const C8 = 59;
    const D1 = 4; const D2 = 12; const D3 = 20; const D4 = 28; const D5 = 36; const D6 = 44; const D7 = 52; const D8 = 60;
    const E1 = 5; const E2 = 13; const E3 = 21; const E4 = 29; const E5 = 37; const E6 = 45; const E7 = 53; const E8 = 61;
    const F1 = 6; const F2 = 14; const F3 = 22; const F4 = 30; const F5 = 38; const F6 = 46; const F7 = 54; const F8 = 62;
    const G1 = 7; const G2 = 15; const G3 = 23; const G4 = 31; const G5 = 39; const G6 = 47; const G7 = 55; const G8 = 63;
    const H1 = 8; const H2 = 16; const H3 = 24; const H4 = 32; const H5 = 40; const H6 = 48; const H7 = 56; const H8 = 64;
    */

    public $name;
    public $piece;
    public $color;

    private static $colorScheme = [
        [self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE],
        [self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK],
        [self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE],
        [self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK],
        [self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE],
        [self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK],
        [self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE],
        [self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK,self::SQUARE_COLOR_WHITE,self::SQUARE_COLOR_BLACK],
    ];

    public static function getColor($row,$column)
    {
        if (isset(static::$colorScheme[$row][$column])) {
            return static::$colorScheme[$row][$column];
        }
        return null;
    }

}