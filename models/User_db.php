<?php
namespace app\models;

use yii\db\ActiveRecord;

class User_db extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    public $permissions;
    public $id;
    public $email;
    public $password;
    public $authKey;
    public $accessToken;

    public static function tableName()
    {
        return 'user';
    }

}