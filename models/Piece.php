<?php

namespace app\models;


use yii\base\Model;

class Piece extends Model
{
    const ROLE_PAWN = 1;
    const ROLE_BISHOP = 2;
    const ROLE_KNIGHT = 3;
    const ROLE_ROOK = 4;
    const ROLE_QUEEN = 5;
    const ROLE_KING = 6;

    const COLOR_WHITE = 1;
    const COLOR_BLACK = 2;


    public $role;

    public $color;

    /* coordinates */
    public $row;
    public $column;

    public function getAllowedMoves($position,$onlyCaptureFlag = false)
    {
        switch ($this->role) {
            case self::ROLE_PAWN:
                return $this->getAllowedMovesPawn($position,$onlyCaptureFlag);
            case self::ROLE_BISHOP:
                return $this->getAllowedMovesBishop($position);
            case self::ROLE_KNIGHT:
                return $this->getAllowedMovesKnight($position);
            case self::ROLE_ROOK:
                return $this->getAllowedMovesRook($position);
            case self::ROLE_QUEEN:
                return $this->getAllowedMovesQueen($position);
            case self::ROLE_KING:
                return $this->getAllowedMovesKing($position);
            default:
                return [];
        }
    }

    public function isValidMove($row, $column, $position) {
        foreach ($this->getAllowedMoves($position) as $move) {
            if ($move[0] == $row && $move[1] == $column) return true;
        }
        return false;
    }

    private function getAllowedMovesPawn($position,$onlyCaptureFlag)
    {
        $moves = [];
        $coefficients = [self::COLOR_WHITE => 1, self::COLOR_BLACK => -1];
        $coefficient = $coefficients[$this->color];
        $newRow = $this->row+$coefficient;
        for ($i = -1; $i <=1; $i++) {
            $newColumn = $this->column + $i;
            if ($newRow <= 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) continue;
            $newSquare = $position[$newRow][$newColumn];
            if (($i == -1 || $i == 1)) {
                if ($newSquare->piece && $this->color != $newSquare->piece->color) {
                    $moves[] = [$newRow,$newColumn];
                }
            }
            else {
                if (!$onlyCaptureFlag) $moves[] = [$newRow,$newColumn];
            }
        }
        if ($this->color == self::COLOR_WHITE && $this->row == 1 && !$onlyCaptureFlag) {
            $moves[] = [$this->row+2,$this->column];
        }
        elseif ($this->color == self::COLOR_BLACK && $this->row == 6 && !$onlyCaptureFlag) {
            $moves[] = [$this->row-2,$this->column];
        }
        return $moves;
    }

    private function getAllowedMovesBishopOld($position)
    {
        $moves = [];
        for ($i = -1*$this->row; $i <= 7-$this->row; $i++) {
            for ($j = -1*$this->column; $j <= 7-$this->column; $j++) {
                if (abs($i) != abs($j) || ($i == 0 && $j == 0)) continue;
                $newRow = $this->row + $i;
                $newColumn = $this->column + $j;
                if ($newRow <= 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) continue;

                $moves[] = [$newRow, $newColumn];
            }
        }
        return $moves;
    }

    private function getAllowedMovesBishop($position)
    {
        $moves = [];
        $directions = [[1,1],[1,-1],[-1,1],[-1,-1]];
        foreach ($directions as $direction) {
            for ($i = 1; $i <= 7; $i++) {
                $newRow = $this->row + $direction[0]*$i;
                $newColumn = $this->column + $direction[1]*$i;
                if ($newRow <= 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) break;
                $newSquare = $position[$newRow][$newColumn];
                if ($newSquare->piece ) {
                    if ($this->color != $newSquare->piece->color) {
                        $moves[] = [$newRow, $newColumn];
                    }
                    break;
                }
                $moves[] = [$newRow, $newColumn];
            }
        }
        return $moves;
    }

    private function getAllowedMovesKnight($position)
    {
        $moves = [];
        for ($i = -2; $i <= 2; $i++) {
            for ($j = -2; $j <= 2; $j++) {
                if ($i == 0 || $j == 0) continue;
                if (abs($i) == abs($j)) continue;
                $newRow = $this->row + $i;
                $newColumn = $this->column + $j;
                if ($newRow < 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) continue;
                $newSquare = $position[$newRow][$newColumn];
                if ($newSquare->piece ) {
                    if ($this->color != $newSquare->piece->color) {
                        $moves[] = [$newRow, $newColumn];
                    }
                    continue;
                }
                $moves[] = [$newRow,$newColumn];
            }
        }
        return $moves;
    }

    private function getAllowedMovesRookOld($position)
    {
        $moves = [];
        for ($i = -1*$this->row; $i <= 7-$this->row; $i++) {
            for ($j = -1*$this->column; $j <= 7-$this->column; $j++) {
                if (($i != 0 && $j != 0) || ($i == 0 && $j == 0)) continue;
                $newRow = $this->row + $i;
                $newColumn = $this->column + $j;
                if ($newRow >= 0 && $newRow <= 7 && $newColumn >= 0 && $newColumn <= 7) {
                    $moves[] = [$newRow, $newColumn];
                }
            }
        }
        return $moves;
    }

    private function getAllowedMovesRook($position)
    {
        $moves = [];
        $directions = [[1,0],[-1,0],[0,1],[0,-1]];
        foreach ($directions as $direction) {
            for ($i = 1; $i <= 7; $i++) {
                $newRow = $this->row + $direction[0]*$i;
                $newColumn = $this->column + $direction[1]*$i;
                if ($newRow <= 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) break;
                $newSquare = $position[$newRow][$newColumn];
                if ($newSquare->piece ) {
                    if ($this->color != $newSquare->piece->color) {
                        $moves[] = [$newRow, $newColumn];
                    }
                    break;
                }
                $moves[] = [$newRow, $newColumn];
            }
        }
        return $moves;
    }

    private  function getAllowedMovesQueenOld($position)
    {
        $moves = [];
        for ($i = -1*$this->row; $i <= 7-$this->row; $i++) {
            for ($j = -1*$this->column; $j <= 7-$this->column; $j++) {
                if ($i == 0 && $j == 0) continue;
                if (abs($i) != abs($j) && $i != 0 && $j != 0) continue;
                $newRow = $this->row + $i;
                $newColumn = $this->column + $j;
                if ($newRow >= 0 && $newRow <= 7 && $newColumn >= 0 && $newColumn <= 7) {
                    $moves[] = [$newRow, $newColumn];
                }
            }
        }
        return $moves;
    }

    private  function getAllowedMovesQueen($position)
    {
        $moves = [];
        $directions = [[1,0],[-1,0],[0,1],[0,-1],[1,1],[1,-1],[-1,1],[-1,-1]];
        foreach ($directions as $direction) {
            for ($i = 1; $i <= 7; $i++) {
                $newRow = $this->row + $direction[0]*$i;
                $newColumn = $this->column + $direction[1]*$i;
                if ($newRow <= 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) break;
                $newSquare = $position[$newRow][$newColumn];
                if ($newSquare->piece ) {
                    if ($this->color != $newSquare->piece->color) {
                        $moves[] = [$newRow, $newColumn];
                    }
                    break;
                }
                $moves[] = [$newRow, $newColumn];
            }
        }
        return $moves;
    }

    private function getAllowedMovesKing($position)
    {
        $moves = [];
        for ($i = -1; $i <= 1; $i++) {
            for ($j = -1; $j <= 1; $j++) {
                if ($i == 0 && $j == 0) continue;
                $newRow = $this->row + $i;
                $newColumn = $this->column + $j;
                if ($newRow <= 0 || $newRow > 7 || $newColumn < 0 || $newColumn > 7) continue;
                $moves[] = [$newRow,$newColumn];
            }
        }
        return $moves;
    }


}